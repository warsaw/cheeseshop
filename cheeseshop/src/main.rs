// Experimental --and simple!-- Python repository implementation.
//
// Download API:
// https://packaging.python.org/en/latest/specifications/simple-repository-api/
//
// Upload API:
// https://peps.python.org/pep-0694
//
// The upload API is technically not standardized, and all implementations
// essentially have to reverse engineer the API:
// https://peps.python.org/pep-0694/#status-quo
//
// PEP 694 is the proposed upload standard API, and we'll play around with it here.

use axum::{Router, extract, routing::get};
use clap::Parser;
use lazy_static::lazy_static;
use mailparse::{MailHeaderMap, parse_headers};
use maud::{html, Markup};
use regex::Regex;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::{Component, Path, PathBuf};
use zip::ZipArchive;


#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    /// Directory to scan for incoming wheels and sdists on start up.
    #[arg(short, long)]
    incoming_dir: Option<PathBuf>,

    /// The root file system path for storing artifacts.
    artifact_root: Option<PathBuf>,
}


#[derive(Clone)]
struct PackageMetadata {
    // Non-normalized package name as recorded in the package metadata.
    name: String,
    // Normalized package name.
    normalized: String,
    version: String,
    path: PathBuf,
}


/// Scan the incoming folder for wheels.  Return a vector of wheel paths.
fn scan_incoming(incoming_dir: PathBuf) -> Vec<PathBuf> {
    let mut wheels = Vec::new();

    for entry in incoming_dir.read_dir().expect("read_dir call failed") {
        if let Ok(entry) = entry {
            if let Some(extension) = entry.path().extension() {
                if extension == "whl" {
                    wheels.push(entry.path());
                }
            }
        }
    }
    wheels
}


/// Match file names in the zip file until we find the *.dist-info/METADATA file.
fn is_metadata_file(filename: &str) -> bool {
    let mut components = Path::new(filename).components();
    if let Some(part) = components.next() {
        if let Component::Normal(part0) = part {
            if let Some(extension) = Path::new(part0).extension() {
                if extension == "dist-info" {
                    if let Some(part) = components.next() {
                        if let Component::Normal(part0) = part {
                            return part0 == "METADATA";
                        }
                    }
                }
            }
        }
    }
    return false;
}


/// Normalize the package name according to the following rules:
/// https://packaging.python.org/en/latest/specifications/name-normalization/#name-normalization
fn normalize_package_name(incoming: &String) -> String {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"[-_.]+").unwrap();
    }
    RE.replace_all(&incoming, "-").to_ascii_lowercase()
}


/// Extract the metadata from a wheel file.
fn scan_wheel(path: PathBuf) -> Option<PackageMetadata> {
    let file = File::open(&path).map_err(|e| {
        eprintln!("Cannot open wheel file: {}", e);
        e
    }).ok()?;

    let reader = BufReader::new(file);

    let mut zip = ZipArchive::new(reader).map_err(|e| {
        eprintln!("Cannot read zip file: {}", e);
        e
    }).ok()?;

    let mut found = None;

    // Find the <pkg>-<version>.dist-info/METADATA file and read some basic stuff from it.  It would be better
    // to know the package name and version rather than search for it, and it would probably be better to read
    // this into a structure the way uv does, so let's convince them to crate-ify their code!
    for i in 0..zip.len() {
        let file = zip.by_index(i).map_err(|e| {
            eprintln!("Error reading file in zip archive: {}", e);
            e
        }).ok()?;

        if is_metadata_file(file.name()) {
            found = Some(String::from(file.name()));
            break;
        }
    }

    let metadata_file = match found {
        None => {
            println!("No METADATA file found");
            return None;
        },
        Some(metadata_file) => metadata_file
    };

    let mut zipfile = match zip.by_name(&metadata_file) {
        Ok(zipfile) => zipfile,
        Err(e) => {
            eprintln!("Error accessing metadata in zip: {}", e);
            return None;
        }
    };

    println!("found: {:?}", zipfile.name());

    let mut contents = String::new();
    if zipfile.read_to_string(&mut contents).is_err() {
        eprintln!("Couldn't read metadata from zip file");
        return None;
    }

    //println!("Metadata:\n{contents}");
    let bytes = contents.as_bytes();
    let (headers, _) = parse_headers(bytes).map_err(|e| {
        eprintln!("Error parsing headers: {}", e);
        e
    }).ok()?;

    // For now, get just the name and version.
    let name = headers.get_first_value("name");
    let version = headers.get_first_value("version");

    //println!("{:?} at {:?}", name, version);

    if let (Some(name), Some(version)) = (name, version) {
        let normalized = normalize_package_name(&name);

        return Some(PackageMetadata {
            name,
            normalized,
            version,
            path,
        });
    }
    else {
        return None;
    }
}


#[derive(Clone)]
struct AppState {
    metadatas: Vec<PackageMetadata>,
}


async fn simple_root(extract::State(state): extract::State<AppState>) -> Markup {
    // The documentation isn't explicit about whether the anchor text should be the normalized package name or
    // not, but PyPI does *not* normalize the text, so we don't either.  The link path must be normalized
    // though.  <https://packaging.python.org/en/latest/specifications/simple-repository-api/#base-html-api>
    html! {
        (maud::DOCTYPE)
            html {
                head {
                    meta name="pypi:repository-version" content="1.1";
                    title { "Simple index" }
                }
                body {
                    @for metadata in &state.metadatas {
                        a href={ "/simple/" (metadata.normalized) "/" } { (metadata.name) }
                    }
                }
            }
    }
}


async fn package_page(
    extract::State(state): extract::State<AppState>,
    extract::Path(package_name): extract::Path<String>,
) -> Markup {
    let normalized = normalize_package_name(&package_name);

    // Find all the metadata entries matching the requested package.
    //
    // TODO: Replace this brute force algorithm with something more efficient in the future, y'know, like a
    //       database query or a map.
    // TODO: Sort using PEP 440 version specifiers: https://peps.python.org/pep-0440/
    // TODO: Adding "Yank" support.

    let mut matching: Vec<String> = Vec::new();

    for metadata in state.metadatas {
        if metadata.normalized == normalized {
            if let Some(filename) = metadata.path.file_name() {
                if let Some(filename) = filename.to_str() {
                    matching.push(String::from(filename));
                }
            }
            // I guess if we couldn't determine a last path component, let's skip this wheel.
        }
    }

    html! {
        (maud::DOCTYPE)
            html {
                head {
                    meta name="pypi:repository-version" content="1.1";
                    title { "Links for " (normalized) }
                }
                body {
                    h1 { "Links for " (normalized) }
                    @for filename in &matching {
                        a href={ "/artifacts/" (filename) } { (filename) }
                        br;
                    }
                }
            }
    }
}


#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let cli = Cli::parse();

    let mut state = AppState {
        metadatas: Vec::new()
    };


    let incoming_wheels: Vec<PathBuf> = match cli.incoming_dir {
        None => Vec::new(),
        Some(incoming_dir) => scan_incoming(incoming_dir)
    };
    for entry in incoming_wheels {
        println!("Scanning {:?}", entry);
        if let Some(metadata) = scan_wheel(entry) {
            state.metadatas.push(metadata);
        }
    }

    // for metadata in &state.metadatas {
    //     println!("Package {} @ version {}", metadata.name, metadata.version);
    // }

    // Download API:
    // https://packaging.python.org/en/latest/specifications/simple-repository-api/
    let app = Router::new()
        // Root index.
        .route("/simple", get(simple_root))
        .route("/simple/:package/", get(package_page))
        .route("/artifacts/:whlname/", get(wheel_file))
        .with_state(state);

    // Run our app with hyper, listening globally on port 8080.
    let listener = tokio::net::TcpListener::bind("127.0.0.1:8080").await?;
    let local_addr = listener.local_addr()?;
    println!("Running on {}", local_addr);

    // Serve
    axum::serve(listener, app).await.unwrap();
    Ok(())
}
