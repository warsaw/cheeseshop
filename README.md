# Experimentation

Super experimental implementation of the PyPI core repository ... in Rust!

The main reason I am writing this is to learn Rust for reals, after having read the Rust book and playing
around with the language at least 3 times.  So, contributions are welcome, but if your MR does some fancy Rust
stuff I don't know about yet, be prepared to get peppered with questions!

Second, I want to build a really light-weight (i.e. _no_ web UI) knockoff of PyPI.  I'm still figuring out the
full scope of what I want to build, but these features at least are, or will be included:

* Scan a directory for wheels; parse out their `.dist-info/METADATA` files, and capture the name and version
  of each package found.  **Prototype working**.
* Implement the [simple repository
  API](https://packaging.python.org/en/latest/specifications/simple-repository-api/). **Partially working**
* Expose [http://localhost:8080/simple](http://localhost:8080/simple) to provide a listing of the top-level
  index, with links to package pages.  **Prototype working**
* Serve package pages with version links.  **Not started**
* Redirects for package name normalization and required trailing slash in links. **Not started**
* Implement accepted [PEP 658](https://peps.python.org/pep-0658/) - Serve Distribution Metadata in the Simple
  Repository API.  **Not started**
* Implement unstandardized "status quo" or "legacy" [upload
  API](https://peps.python.org/pep-0694/#status-quo).  **Not started**
* Implement draft [PEP 694](https://peps.python.org/pep-0694/) - Upload 2.0 API for Python Package
  Repositories.  **Not started**

A major milestone will be when `pip` for downloading and `twine` (or equivalent) for uploading work.

Other ideas once the above are working:
* Pluggable storage backend for wheel storage
* sdist support
* user model (including org accounts, namespace, etc.)
* Proxying from upstream indexes

# Trying it out

## Requirements

In addition to the Rust toolchain, you'll also need `cmake`.

## Set up

1. Drop a bunch of `.whl` files in an "incoming directory" of your choice.  There is no default.
2. `cd cheeseshop`
3. `cargo run -- -i <incoming-whldir>`

This will start an HTTP server listening on [localhost:8080](http://localhost:8080).

4. `curl --get http://localhost:8080/simple`

This will print the HTML index page!

# Other notes

## Download the real index in JSON format

```bash
$ curl --get https://pypi.org/simple/ --header "Accept: application/vnd.pypi.simple.v1+json" -o simple.json
```

## Opportunities for cooperation

The [uv](https://github.com/astral-sh/uv) project looks super interesting and I've poked around their code a
bit to get some ideas, but haven't straight up copied anything of their yet. That said, I think there's some
good opportunities to work together, especially in building crates for parsing wheel files, scanning metadata,
package name normalization, etc.  I hope to reach out to the fine folks at [Astral](https://astral.sh/) to
look into working together.

## License

Apache 2.0 as described in the [LICENSE](LICENSE) file.
